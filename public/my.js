﻿window.addEventListener("DOMContentLoaded", function () {
    const C1 = 200;
    const C2 = 250;
    const C3 = 100;
    const Nothing = 0;
    const R1 = 20;
    const R2 = 10;
    const R3 = 50;
    const CHECKBOX = 50;
    let price = C2;
    let extraPrice = R1;
    let result;
    let calc = document.getElementById("calcin");
    let Num = document.getElementById("num");
    let resultSpan = document.getElementById("result");
    let Sel = document.getElementById("sel");
    let radiosDiv = document.getElementById("my-radio-div");
    let radios = document.querySelectorAll("#my-radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("checkbox-div");
    let checkbox = document.getElementById("checkbox");
    Sel.addEventListener("change", function (event) {
        let option = event.target;
        if (option.value === "1") {
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = Nothing;
            price = C1;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = R1;
            price = C2;
            document.getElementById("my-radio1").checked = true;
        }
        if (option.value === "3") {
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = Nothing;
            price = C3;
            checkbox.checked = false;
        }
    });
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            let radio = event.target;
            if (radio.value === "r1") {
                extraPrice = R1;
            }
            if (radio.value === "r2") {
                extraPrice = R2;
            }
            if (radio.value === "r3") {
                extraPrice = R3;
            }
        });
    });
    checkbox.addEventListener("change", function () {
        if (checkbox.checked) {
            extraPrice = CHECKBOX;
        } else {
            extraPrice = Nothing;
        }
    });
    calc.addEventListener("change", function () {
        if (Num.value < 1) {
            Num.value = 1;
        }
        result = (price + extraPrice) * Num.value;
        resultSpan.innerHTML = result;
    });
});
